import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";

@Component({
  selector: "app-login-form",
  templateUrl: "./login-form.component.html",
  styleUrls: ["./login-form.component.scss"],
})
export class LoginFormComponent {
  @Input() errorMessage = "";
  @Output() Login: EventEmitter<{ username: string; password: string }> =
    new EventEmitter();
  username = "";
  password = "";
  submitted = false;

  handleFormSubmit(): void {
    this.submitted = true;

    if (!this.username || !this.password) {
      return;
    }

    this.Login.emit({ username: this.username, password: this.password });
  }
}
