import { Component, ViewChild } from "@angular/core";
import { ButtonComponent } from "./button.component";
import { IonicModule } from "@ionic/angular";

@Component({
  template: ` <app-button> Click Me </app-button> `,
})
class WrapperComponent {}

describe("ButtonComponent", () => {
  it("can mount using WrapperComponent", () => {
    cy.mount(WrapperComponent, {
      declarations: [ButtonComponent],
      imports: [IonicModule.forRoot()],
    });
    cy.get("button").contains("Click Me");
  });

  it("can mount using template syntax", () => {
    cy.mount("<app-button>Click Me</app-button>", {
      declarations: [ButtonComponent],
      imports: [IonicModule.forRoot()],
    });
    cy.get("button").contains("Click Me");
  });

  it("when button is clicked, should call onClick", () => {
    cy.mount(ButtonComponent, {
      imports: [IonicModule.forRoot()],
    }).then((response) => {
      cy.spy(response.component.Click, "emit").as("Click");
      cy.get("button").click();
      cy.get("@Click").should("have.been.called");
    });
  });

  it("when button is clicked, should call onClick using autoSpyOutputs", () => {
    cy.mount(ButtonComponent, {
      autoSpyOutputs: true,
      imports: [IonicModule.forRoot()],
    });
    cy.get("button").click();
    cy.get("@ClickSpy").should("have.been.called");
  });
});
