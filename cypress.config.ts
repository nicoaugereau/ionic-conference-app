import { defineConfig } from "cypress";

export default defineConfig({
  env: {
    screenshotsFolder: "./cypress/snapshots/actual",
    trashAssetsBeforeRuns: true,
    video: false,
    failSilently: false,
    ALWAYS_GENERATE_DIFF: true,
  },
  video: false,
  videoUploadOnPasses: false,
  screenshotOnRunFailure: false,
  defaultCommandTimeout: 10000,
  fixturesFolder: "e2e/fixtures",
  screenshotsFolder: "e2e/screenshots",
  trashAssetsBeforeRuns: true,
  e2e: {
    baseUrl: "http://localhost:8100",
    specPattern: ["e2e/specs/**/*.{feature,features,ts,tsx}"],
    experimentalStudio: true,
    experimentalWebKitSupport: true,
    testIsolation: false,
    setupNodeEvents(on, config) {
      const browserify = require("@cypress/browserify-preprocessor");
      const options = {
        ...browserify.defaultOptions,
        typescript: require.resolve("typescript"),
      };
      const cucumber = require("cypress-cucumber-preprocessor").default;
      on("file:preprocessor", cucumber(options));

      // Cypress visual testing
      const getCompareSnapshotsPlugin = require("cypress-visual-testing/dist/plugin");
      getCompareSnapshotsPlugin(on, config);

      // Add custom browsers to Cypress
      const addCustomBrowsers = require("./cypress/support/customBrowsers");
      addCustomBrowsers(config);

      /**
       * Browser configuration
       * SEE : https://www.chromium.org/developers/how-tos/run-chromium-with-flags/
       * TAGS LIST : https://peter.sh/experiments/chromium-command-line-switches/
       */
      on("before:browser:launch", (browser, launchOptions) => {
        console.log(
          "launching browser %s is headless? %s",
          browser.name,
          browser.isHeadless
        );
        console.log(
          "the viewport size is %d x %d",
          config.viewportWidth,
          config.viewportHeight
        );
        // résolution machine distante supposé de 1680 * 1050
        // console.log('setting the browser window size to %d x %d', width, height)

        if (browser.family === "chromium" && browser.name !== "electron") {
          //launchOptions.args.push('--auto-open-devtools-for-tabs')
          //launchOptions.args.push('--start-maximized')
          //launchOptions.args.push('--start-fullscreen')
          //launchOptions.args.push('--cast-initial-screen-width=' + config.viewportWidth)
          //launchOptions.args.push('--cast-initial-screen-height=' + config.viewportHeight)
          launchOptions.args.push(
            `--window-size=${config.viewportWidth},${config.viewportHeight}`
          );
          //launchOptions.args.push('--force-device-scale-factor=1')
          launchOptions.args.push("--disable-dev-shm-usage");
          launchOptions.args.push("--ignore-certificate-errors-spki-list");
          launchOptions.args.push("--noerrdialogs");
          launchOptions.args.push("--disable-popup-blocking");
          launchOptions.args.push("--allow-insecure-localhost");
          //launchOptions.args.push('--no-sandbox')
          //launchOptions.args.push('--disable-setuid-sandbox')
          //launchOptions.args.push('--incognito') // navigation privée
          launchOptions.preferences.frame = false;
          launchOptions.preferences.useContentSize = true;
          // change download directory
          // https://docs.cypress.io/api/plugins/browser-launch-api.html#Change-download-directory
          // https://github.com/cypress-io/cypress/issues/949
          launchOptions.preferences.default.profile = {
            default_content_settings: { popups: 0 },
            default_content_setting_values: { automatic_downloads: 1 },
          };
          launchOptions.preferences.default["download"] = {
            default_directory: config.downloadsFolder,
          };

          return launchOptions;
        }

        if (browser.family === "firefox") {
          //launchOptions.args.push('-devtools')
          launchOptions.args.push(`--width=${config.viewportWidth}`);
          launchOptions.args.push(`--height=${config.viewportHeight}`);
          launchOptions.preferences["browser.download.dir"] =
            config.downloadsFolder;
          launchOptions.preferences["browser.download.folderList"] = 2;
          // needed to prevent download prompt for text/csv files.
          launchOptions.preferences["browser.helperApps.neverAsk.saveToDisk"] =
            "text/csv";

          return launchOptions;
        }
        if (browser.name === "electron") {
          // launchOptions.preferences is a `BrowserWindow` `options` object
          launchOptions.preferences.width = config.viewportWidth;
          launchOptions.preferences.height = config.viewportHeight;
          //launchOptions.preferences.frame = false
          launchOptions.preferences.useContentSize = true;
          launchOptions.preferences.fullscreen = false;
          //launchOptions.preferences.darkTheme = true

          launchOptions.preferences.webPreferences.session = {
            downloaditem: config.downloadsFolder,
          };

          return launchOptions;
        }

        if (browser.family === "webkit" && browser.name == "safari") {
          // auto open devtools
          launchOptions.args.push("--auto-open-devtools-for-tabs");
          launchOptions.args.find(
            (arg) => arg.slice(0, 23) === "--remote-debugging-port"
          );

          return launchOptions;
        }
      });

      on("after:screenshot", (details) => {
        const { rename } = require("fs-extra");
        // /!\ Le nom doit être identique avec celui défini dans support/index.js pour l'inclure dans le rapport de tests
        // /!\ Doit aussi être identique à la regex cucumber-report.js pour les tests feature

        // Type de spec (fichier) : .js ou .feature
        // var specType = details.specName.split('.').pop()
        // Nom de la spec
        var newSpecName = details.specName
          .replace(/^.*[\\\/]/, "")
          .split(".")
          .slice(0, -1);
        // Nom du projet (sesame / b2c / odisea)
        //var app = details.specName.match(/^.*?[^\/]*/)

        // Renomme Screenshot si test non Visual Testing
        // Take snapshots : type=base
        // Test regression : type=actual
        //if (config.env.type == undefined || config.env.hasOwnProperty('type') == false) {
        // Pour tout test failed, renomme le screenshot
        if (details.path.includes("(failed)")) {
          details.scaled = true;
          //details.dimensions.height = config.viewportHeight
          //details.dimensions.width = config.viewportWidth
          // var newPath = `${config.screenshotsFolder}/${details.specName}/failedTest.png`
          var newPath = `${config.screenshotsFolder}/${details.specName}/${newSpecName[0]} (failed).png`;
          newPath = newPath.replace(/\\/g, "/");

          return new Promise((resolve, reject) => {
            rename(details.path, newPath, (err) => {
              if (err) return reject(err);
              resolve({ path: newPath });
            });
          });
        }
      });

      return config;
    },
  },
  component: {
    devServer: {
      framework: "angular",
      bundler: "webpack",
    },
    setupNodeEvents(on, config) {
      //on("dev-server:start", webpack(options));
    },
    specPattern: "**/*.cy.ts",
  },
});
