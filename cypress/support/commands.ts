// ***********************************************
// This example namespace declaration will help
// with Intellisense and code completion in your
// IDE or Text Editor.
// ***********************************************
// declare namespace Cypress {
//   interface Chainable<Subject = any> {
//     customCommand(param: any): typeof customCommand;
//   }
// }
//
// function customCommand(param: any): void {
//   console.warn(param);
// }
//
// NOTE: You can use it like so:
// Cypress.Commands.add('customCommand', customCommand);
//
// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
import compareSnapshotCommand from "cypress-visual-testing/dist/command";
compareSnapshotCommand();

import { Storage } from "@ionic/storage";
const storage = new Storage();
storage.create();

Cypress.Commands.add("bypassTutorial", () => {
  cy.log("bypass tutorial");
  storage.set("ion_did_tutorial", true);
});

Cypress.Commands.add("enableTutorial", () => {
  cy.log("enable tutorial");
  cy.visit("/", {
    onBeforeLoad() {
      storage.set("ion_did_tutorial", false);
    },
  });
});

/**
 * Configuration du navigateur en mode mobile
 */
Cypress.Commands.add("device", (device, mode = null) => {
  cy.fixture("devices").then((devices) => {
    let d = devices.sizes.hasOwnProperty(device);
    let configOptions =
      d == true
        ? mode != ""
          ? mode == "paysage" && device.includes("iphone")
            ? {
                viewportWidth: devices.sizes[device].viewportHeight,
                viewportHeight: devices.sizes[device].viewportWidth,
                //userAgent: devices.sizes[device].useragent
              }
            : mode == "portrait" && !device.includes("iphone")
            ? {
                viewportWidth: devices.sizes[device].viewportHeight,
                viewportHeight: devices.sizes[device].viewportWidth,
                //userAgent: devices.sizes[device].useragent
              }
            : {
                viewportWidth: devices.sizes[device].viewportWidth,
                viewportHeight: devices.sizes[device].viewportHeight,
                //userAgent: devices.sizes[device].useragent
              }
          : // si viewport trouvé mais sans mode, mode par défaut du device
            {
              viewportWidth: devices.sizes[device].viewportWidth,
              viewportHeight: devices.sizes[device].viewportHeight,
            }
        : // si viewport non trouvé ou non renseigné, mode par défaut
          { viewportWidth: 1280, viewportHeight: 720 };

    Cypress.config("viewportHeight", configOptions.viewportHeight);
    Cypress.config("viewportWidth", configOptions.viewportWidth);
    //Cypress.config('userAgent', configOptions.userAgent)
  });
});

declare global {
  namespace Cypress {
    interface Chainable {
      /**
       * Custom command to bypass the tutorial screens in the app
       */
      bypassTutorial(): Chainable<JQuery<HTMLElement>>;
      /**
       * Custom command to avoid bypassing the tutorial screens in the app
       */
      enableTutorial(): Chainable<JQuery<HTMLElement>>;
      /**
       * Custon command
       * @param mode
       */
      environnement(env: string): Chainable<void>;
      /**
       * Custom command for device testing
       * @param device
       * @param mode
       */
      device(device: string, mode?: string): Chainable<any>;
    }
  }
}
