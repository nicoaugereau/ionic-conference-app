/// <reference types="cypress" />
/**
 * Cypress customize browsers
 * https://docs.cypress.io/guides/guides/launching-browsers#Customize-available-browsers
 *
 * Browsers à déposer dans le répertoire racine du projet sesame-tests : ./browsers
 */
var customList;
function addCustomBrowsers(config) {
  customList = [
    {
      name: "Brave",
      family: "chromium",
      channel: "stable",
      displayName: "Brave Browser",
      version: "1.52.117",
      path: "/Applications/Brave Browser.app/Contents/MacOS/Brave Browser",
      majorVersion: "1.52",
    },
    {
      name: "Chromium",
      family: "chromium",
      channel: "stable",
      displayName: "Chromium",
      version: "115.0.5759.0",
      path: "/Users/nicolasaugereau/Downloads/Developpement/NodeJS/projets/cypress-meetup/ionic-conference-app/browsers/chromium/Chromium.app/Contents/MacOS/Chromium",
      majorVersion: "115.0",
    },
  ];
  return (config.browsers = config.browsers.concat(customList));
}

module.exports = addCustomBrowsers;
