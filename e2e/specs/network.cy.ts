describe("Mock et erreurs réseau", () => {
  const environment = "local";
  const errorMsg =
    "Oops! Notre site est momentanément indisponible. Revenez un peu plus tard.";

  it("Le site est en ligne", () => {
    cy.visit("/");
    cy.get('[data-testid="tab-schedule"]').should("be.visible");
  });

  it("Le site est en ligne mais avec des lenteurs réseau", () => {
    /**
     * Preset,download(kb/s),upload(kb/s),RTT(ms)
     * GPRS,50,20,500
     * Regular 2G,250,50,300
     * Good 2G,450,150,150
     * Regular 3G,750,250,100
     * Good 3G, 1000,750,40
     * Regular 4G, 4000,3000,20
     * DSL 2000, 1000,5
     * WiFi 30000,15000,2
     */
    cy.intercept("GET", "/assets/data/*", (req) => {
      req.reply({ delay: 500, throttleKbps: 50 }); // milliseconds // to simulate a GPRS connection
    }).as("slowNetwork");

    cy.visit("/");

    cy.wait("@slowNetwork");
    cy.get('[data-testid="tab-schedule"]').should("be.visible");
  });

  it("Simuler une erreur serveur", () => {
    cy.intercept("GET", "/assets/data/*", {
      statusCode: 500,
      body: "Server error",
      headers: { "x-not-found": "true" },
    }).as("getServerFailure");

    cy.visit("/");

    cy.wait("@getServerFailure");
    cy.get("@getServerFailure").then((res) => {
      expect(res.response.statusCode).eq(500);
    });
  });

  it("Simuler une erreur réseau", () => {
    cy.intercept("GET", "/assets/data/*", { forceNetworkError: true }).as(
      "getNetworkFailure"
    );

    cy.visit("/");

    cy.wait("@getNetworkFailure");
    cy.wait("@getNetworkFailure").then((res) => {
      expect(res.error.name).eq("Error");
    });
  });

  it("Mock des données Schedules et Speakers", () => {
    // Schedule Tab
    cy.intercept("GET", "/assets/data/*", {
      fixture: "mock-datas.json",
    }).as("mockedDatas");

    cy.visit("/");

    cy.wait("@mockedDatas");

    cy.wait(2000);

    // Speakers Tab
    cy.get('[data-testid="tab-speakers"]').click();

    cy.contains("Antonin Founeau");
  });
});
